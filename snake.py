#!/usr/bin/env python3
# Snake-curses
# By Dale Magee

import sys,os, curses, curses.panel,time,random

from curses import A_ALTCHARSET,A_BLINK,A_BOLD,A_DIM,A_INVIS,A_NORMAL,A_PROTECT,A_REVERSE,A_STANDOUT,A_UNDERLINE,A_HORIZONTAL,A_LEFT,A_LOW,A_RIGHT,A_TOP,A_VERTICAL,A_CHARTEXT

random.seed()

class Snake(object):
	#directions
	UP,DOWN,LEFT,RIGHT = range(4)
	
	#different heads for different directions.
	# it's important that we use the same order as the directions
	"""
	headchars = [
		"^",
		"V",
		"<",
		">",
		
	]
	"""
	
	headchars = "⏶⏷⏴⏵"
	
	#chars used for body.
	# this is a bit complex and depends on where
	# adjacent pieces are
	# this arraw is in a specific order
	bodychars = "║═╔╗╚╝"
	#bodychars = "┃━┏┓┗┛"
	#bodychars = "│─╭╮╰╯"
	
	foodchars = "***++--321"
	#foodchars = ["*","+","-","/","|","\\","-","/","|","\\","-"]
	#foodchars = ["*","+","!"]
	#how long (seconds) before food goes away:
	foodtime = 10
	#max food on screen at once:
	maxfood = 3
		
	def __init__(self):
		
		#an array of coordinates occupied by the snake
		#self.positions = [ (2,2),(2,3),(2,4) ]
		
		initpos = (5,20)
		initdir = self.RIGHT
		initlen = 3
		
		nx= random.randrange(5,curses.COLS-10,1)
		ny= random.randrange(2,curses.LINES-5,1)
		
		self.positions = self.buildsnake((ny,nx),initdir,initlen)
		
		self.food = []
		self.addfood()
		
		self.direction = initdir
		
		self.nextfood = time.time() + random.randrange(1,int(round(self.foodtime / 2)))

	def addfood(self):
		"""
		Add a new piece of food.
		Ensures new coords are sensible, i.e not on the snake.
		"""
		
		ok = False
		
		while not ok:
			nx= random.randrange(1,curses.COLS-2,1)
			ny= random.randrange(1,curses.LINES-2,1)
			ok = True
			for px,py in self.positions:
				if nx == px and ny == py:
					ok = False
					break
			
		
		self.food.append((ny,nx,time.time()))

	def move(self,interface):
		"""
		Do movement. This involves adding a new coordinate to our position
		list, and (usually) removing one from the end
		
		returns a true/false indicating whether the game should continue or is
			over
		
		"""
		
		y,x=self.getcoord()
		
		#collision checks:
		
		#edge:
		if y <1 or y >= (interface.height-1) or x<1 or x >= (interface.width-1):
			#death!
			return False	
		
		#self:
		for py,px in self.positions:
			if y == py and x == px:
				#oroborous!
				return False
			
		self.positions.append((y,x))
		
		fed=False
		for k,(fy,fx,when) in enumerate(self.food):
			if y == fy and x == fx:
				#got food! add a new one:
				fed=True
				#remove eaten food:
				self.food.pop(k)
				#create new food:
				#todo: ensure it's not on the snake. and maybe not on edges.
				if len(self.food) == 0:
					self.addfood()
				
			
		if not fed:
			#remove end coord:
			end = self.positions.pop(0)
			
		return True
	
	def buildsnake(self,head,direction,length = 3):
		"""
		return an array with data creating the specified snake, ready to be
		 used as self.positions
		"""
		#we populate the array in reverse, starting from the head (which needs
		# be the last element in self.positions)
		ret = []
		pos = head
		#reverse direction because we build the array in reverse:
		direction = self.reverse(direction)
		for idx in range(0,length):
			ret.insert(0,pos)
			pos = self.getcoord(direction,pos)
		return ret
		
	
	def draw(self,interface):
		"""
		draw the snake using the provided interface
		"""
		for k,pos in enumerate(self.positions):
			row,col = pos
			"""
			if k == (len(self.positions)-1):
				char = self.headchars[self.direction]
				attrs = A_NORMAL
				is_head = False
			else:
				char = " "
				attrs = A_REVERSE
			"""
			attrs = A_NORMAL
			char = self.bodychar(k)
			
			interface.addstr(row,col,char,attrs)
		
		if time.time() >= self.nextfood:
			if len(self.food) < self.maxfood:
				self.addfood()
			self.nextfood = time.time() + random.randrange(1,self.foodtime)
			
		for k,(y,x,when) in enumerate(self.food):
			dur = time.time() - when
			if dur > self.foodtime:
				#gone!
				self.food.pop(k)
				self.addfood()
			else:
				idx = int(dur / (self.foodtime / len(self.foodchars)))
				char = self.foodchars[idx]
				interface.addstr(y,x,char,A_BOLD)
		
		txt = "Length: %1d" % len(self.positions)
		i.addstr(i.height-1,2,txt,A_NORMAL)
	
	def bodychar(self,idx):
		"""
		return the appropriate character for the given index 
		in self.positions
		"""
		char = "?" # should never show
		if idx == (len(self.positions)-1):
			#head
			char = self.headchars[self.direction]
		elif idx == 0:
			#end, only one side
			direction = self.getheading(self.positions[0],self.positions[1])
			if direction == self.UP or direction == self.DOWN:
				char = self.bodychars[0]
			else:
				char = self.bodychars[1]
		else:
			#get directions to both adjacent positions:
			d1 = self.getheading(self.positions[idx-1],self.positions[idx])
			d2 = self.getheading(self.positions[idx],self.positions[idx+1])
			if d1 == d2:
				if d1 == self.UP or d1 == self.DOWN:
					char = self.bodychars[0]
				else:
					char = self.bodychars[1]
			elif d1 == self.UP:
				if d2 == self.LEFT:
					char = self.bodychars[2]
				else:
					char = self.bodychars[3]
			elif d1 == self.DOWN:
				if d2 == self.LEFT:
					char = self.bodychars[4]
				else:
					char = self.bodychars[5]
			elif d1 == self.LEFT:
				if d2 == self.UP:
					char = self.bodychars[5]
				else:
					char = self.bodychars[3]
			else:
				if d2 == self.UP:
					char = self.bodychars[4]
				else:
					char = self.bodychars[2]
					
		return char
		
	def reverse(self,direction):
		#return the opposite of the given direction
		if direction == self.UP:
			return self.DOWN
		elif direction == self.DOWN:
			return self.UP
		elif direction == self.LEFT:
			return self.RIGHT
		else:
			return self.LEFT
			
	def getheading(self,coord1,coord2):
		"""
		Return the direction from one coord to another. only U/D/L/R
		are supported
		returns none if coords are the same or at an angle
		coords do not have to be adjacent.
		"""
		y1,x1 = coord1
		y2,x2 = coord2
		if x1 == x2:
			if y2 < y1:
				return self.UP
			elif y2 > y1:
				return self.DOWN
			else: 
				return None
		elif y1 == y2:
			if x1 < x2:
				return self.LEFT
			elif x1 > x2:
				return self.RIGHT
			#no need for else, returns none, caught above
		else:
			return None
		
			
	def getcoord(self,direction = None,coord = None):
		"""
		return the next coordinate the snake will want to occupy 
		based on direction and head posiiton
		"""
		if coord == None:
			y,x = self.positions[-1]
		else:
			y,x = coord
			
		if direction == None:
			direction = self.direction
		
		if direction == Snake.UP:
			y -= 1
		elif direction == Snake.DOWN:
			y += 1
		elif direction == Snake.LEFT:
			x -= 1
		elif direction == Snake.RIGHT:
			x += 1

		return (y,x)	
		

class Interface(object):
	title = "Snake v0.1. By AntiSol"
	def __init__(self):
		self.screen = curses.initscr()
		#curses.def_shell_mode()
		curses.noecho()
		curses.raw()
		curses.cbreak()
		self.screen.keypad(True)
		curses.curs_set(0)
		self.screen.nodelay(True)
		
		self.width = curses.COLS
		self.height = curses.LINES
		
		self.mainwin = curses.newwin(self.height, self.width,0, 0)
		#self.draw_border()
		
	def __del__(self):
		# useful for debugging:
		#time.sleep(3)
		self.shutdown()
		
	def refresh(self):
		"""
		Update windows
		"""
		#self.mainwin.box()
		self.draw_border()
		self.mainwin.refresh()
	
	def draw_border(self):
		self.mainwin.box()
		x = int(round((self.width / 2) - (len(self.title) / 2)))
		self.addstr(0,x,self.title,A_BOLD)
	
	def clear(self):
		self.mainwin.erase()
		#self.draw_border()
	
	def addstr(self,row,col,line,attrs):
		self.mainwin.addstr(row,col,line,attrs)
		
	
		
	def shutdown(self):
		"""
		Shut down the interface, closing ncurses
		"""
		curses.nocbreak()
		self.screen.keypad(False)
		curses.echo()
		#curses.reset_shell_mode()
		curses.endwin()
		
		
		print("\nShutdown.\n")
	
if __name__ == '__main__':
	running = True
	i = Interface()
	snake = Snake()
	
	while running:
		kin = i.screen.getch()
		
		# protip:  this can give a KEY_MOUSE response, and we can then call curses.getmouse()
		if kin == curses.KEY_UP and snake.direction != Snake.DOWN:
			snake.direction = Snake.UP
		elif kin == curses.KEY_DOWN and snake.direction != Snake.UP:
			snake.direction = Snake.DOWN
		elif kin == curses.KEY_LEFT and snake.direction != Snake.RIGHT:
			snake.direction = Snake.LEFT
		elif kin == curses.KEY_RIGHT and snake.direction != Snake.LEFT:
			snake.direction = Snake.RIGHT
		elif kin == 27:
			k = curses.keyname(kin)
			i.shutdown()
			#print("Exited")
			sys.exit(1)
			
		running = snake.move(i)
		i.clear()
		snake.draw(i)	
		i.refresh()
		time.sleep(0.1)
	
	t="G A M E   O V E R"
	for a in range(0,len(t)):
		i.addstr(int(i.height/2),int((i.width / 2) - (len(t)/2))+a,t[a],A_BOLD)
		i.refresh()
		time.sleep(0.1)
		kin = i.screen.getch()
		
	i.screen.nodelay(False)
	kin = i.screen.getch()
	
